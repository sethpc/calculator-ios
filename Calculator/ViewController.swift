//
//  ViewController.swift
//  Calculator
//
//  Created by Chhaileng Peng on 6/2/18.
//  Copyright © 2018 Chhaileng Peng. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblNum: UILabel!
    
    @IBOutlet weak var lblOldnumber: UILabel!
    var num = ""
    var oldNum = ""
    var sign = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func btn1(_ sender: Any) {
        num += "1"
        lblNum.text = num
    }
    
    @IBAction func btn2(_ sender: Any) {
        num += "2"
        lblNum.text = num
    }
    
    @IBAction func btn3(_ sender: Any) {
        num += "3"
        lblNum.text = num
    }
    
    @IBAction func btn4(_ sender: Any) {
        num += "4"
        lblNum.text = num
    }
    
    @IBAction func btn5(_ sender: Any) {
        num += "5"
        lblNum.text = num
    }
    
    @IBAction func btn6(_ sender: Any) {
        num += "6"
        lblNum.text = num
    }
    
    @IBAction func btn7(_ sender: Any) {
        num += "7"
        lblNum.text = num
    }
    
    @IBAction func btn8(_ sender: Any) {
        num += "8"
        lblNum.text = num
    }
    
    @IBAction func btn9(_ sender: Any) {
        num += "9"
        lblNum.text = num
    }
    
    @IBAction func btn0(_ sender: Any) {
        num += "0"
        lblNum.text = num
    }
    
    @IBAction func btnClear(_ sender: Any) {
        lblNum.text = "0"
        lblOldnumber.text = ""
        num = ""
        oldNum = ""
    }
    
    @IBAction func btnplus(_ sender: Any) {
        sign = "+"
        if (oldNum.count > 0){
            let result = String(Float(oldNum)! + Float(num)!)
            oldNum = result
            lblNum.text = "0"
            num = ""
            lblOldnumber.text = result
        }else{
            lblOldnumber.text = num + "+"
            oldNum = num
            num = ""
            lblNum.text = ""
        }
        
    }
    
    @IBAction func Minus(_ sender: Any) {
        sign = "-"
        if (oldNum.count > 0){
            let result = String(Float(oldNum)! - Float(num)!)
            oldNum = result
            lblNum.text = "0"
            num = ""
            lblOldnumber.text = result
        }else{
            lblOldnumber.text = num + "-"
            oldNum = num
            num = ""
            lblNum.text = ""
        }
    }
    
    @IBAction func btnBy(_ sender: Any) {
        sign = "*"
        if (oldNum.count > 0){
            let result = String(Float(oldNum)! * Float(num)!)
            oldNum = result
            lblNum.text = "0"
            num = ""
            lblOldnumber.text = result
        }else{
            lblOldnumber.text = num + "x"
            oldNum = num
            num = ""
            lblNum.text = ""
        }
    }
    
    @IBAction func btnDevice(_ sender: Any) {
        sign = "/"
        if (oldNum.count > 0){
            let result = String(Float(oldNum)! / Float(num)!)
            oldNum = result
            lblNum.text = "0"
            num = ""
            lblOldnumber.text = result
        }else{
            lblOldnumber.text = num + "/"
            oldNum = num
            num = ""
            lblNum.text = ""
        }
    }
    
    @IBAction func btnModulo(_ sender: Any) {
        sign = "%"
        if (oldNum.count > 0){
            let result = String(Float(oldNum)!.truncatingRemainder(dividingBy: Float(num)!))
            oldNum = result
            lblNum.text = "0"
            num = ""
            lblOldnumber.text = result
        }else{
            lblOldnumber.text = num + "%"
            oldNum = num
            num = ""
            lblNum.text = ""
        }
    }
    
    @IBAction func numberMinus(_ sender: Any) {
        
    }
    
    @IBAction func btnResult(_ sender: Any) {
        if sign=="+"{
            let result = String(Float(oldNum)! + Float(num)!)
            lblOldnumber.text = oldNum + " \(sign) " + num
            lblNum.text = result
            num = "0"
            oldNum = result
        }
        else if sign=="-"{
            let result = String(Float(oldNum)! - Float(num)!)
            lblOldnumber.text = oldNum + " \(sign) " + num
            lblNum.text = result
            num = "0"
            oldNum = result
        }
        else if sign=="*"{
            let result = String(Float(oldNum)! * Float(num)!)
            lblOldnumber.text = oldNum + " \(sign) " + num
            lblNum.text = result
            num = "0"
            oldNum = result
        }
        else if sign=="/"{
            let result = String(Float(oldNum)! / Float(num)!)
            lblOldnumber.text = oldNum + " \(sign) " + num
            lblNum.text = result
            num = "0"
            oldNum = result
        }
        else if sign=="%"{
            let result = String(Float(oldNum)!.truncatingRemainder(dividingBy: Float(num)!))
            lblOldnumber.text = oldNum + " \(sign) " + num
            lblNum.text = result
            num = "0"
            oldNum = result
        }
    }
}

